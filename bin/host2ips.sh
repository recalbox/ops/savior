#!/bin/bash -e
SEPARATOR=" "
SERVER=""
while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    --separator)
    SEPARATOR="$2"
    shift
    ;;
    --server)
    SERVER="$2"
    shift
    ;;
    --port)
    PORT="$2"
    shift
    ;;
    *)
    ;;
esac
shift
done

HOST="$1"
if [[ -z "${HOST}" ]]; then
    echo -e "Usage:\n$0 --server DNSSERVER --separator SEPARATOR --port PORT HOST" && exit 1
fi
EXPANDED_HOSTS=""
ADDRESSES="$(nslookup "${HOST}" ${SERVER}| sed -En 's|Address .+: (.*)|\1|p')"
MUSTSEPARATE=0
for IP in ${ADDRESSES}; do
  test ${MUSTSEPARATE} -eq 1 && EXPANDED_HOSTS="${EXPANDED_HOSTS}${SEPARATOR}"
  EXPANDED_HOSTS="${EXPANDED_HOSTS}${IP}"
  if [ -n "${PORT}" ];then
    EXPANDED_HOSTS="${EXPANDED_HOSTS}:${PORT}"
  fi
  MUSTSEPARATE=1
done

echo ${EXPANDED_HOSTS}