#!/bin/bash

CRONDFILE=${CRONDFILE:-/etc/crontabs/root}
OLDIFS=$IFS
IFS=$'\n'
for saviorvar in $(env | grep '^SAVIOR_*');do
  saviorvarname="${saviorvar%=*}"
  groupname="${saviorvarname%_*}"
  for var in _HOST _PORT _CRON _BUCKET; do
    if [ "$(env | grep "^${groupname}$var")" == "" ]; then
      echo "The group $groupname cannot be saved as it do not contain all required env variables."
      exit 1
    fi
  done

  if [[ "$saviorvar" =~ SAVIOR.*TYPE.* ]];then
    typename="${saviorvar%=*}"
    typeval="${saviorvar##*=}"
    varname="${typename%_*}"
    index="${varname##SAVIOR_}"
    hostvar="${varname}_HOST"
    hostval="${!hostvar}"
    portvar="${varname}_PORT"
    portval="${!portvar}"
    cronvar="${varname}_CRON"
    cronval="${!cronvar}"
    bucketvar="${varname}_BUCKET"
    bucketval="${!bucketvar}"
    expandvar="${varname}_EXPAND"
    expandval="${!expandvar}"

    #echo -e "varname = $varname\ntypeval = $typeval\nhostval = $hostval\nportval = $portval\ncronval = $cronval\nindex = $index"

    if [[ -z $cronval || -z $typeval || -z $portval || -z $hostval ]];then echo "Unable to configure the savior for $varname variables" && exit 1; fi
    echo "${cronval} /usr/local/bin/savior.sh --save "${expandval:+--expandHost }"--type \"${typeval}\" --bucket \"${bucketval}\" --host \"${hostval}\" --port \"${portval}\" --name \"${index}\"" >> ${CRONDFILE}
    echo "The savior is ready for : ${index}"
  fi
done
IFS=$OLDIFS
