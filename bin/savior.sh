#!/bin/bash

SAVIORS_DIR=${SAVIORS_DIR:-/usr/local/bin/saviors}
DATA_DIR=${DATA_DIR:-/saviors}

while [[ $# -gt 1 ]]
do
key="$1"
case $key in
    --resurrect)
    RESTORE=YES
    ;;
    --save)
    DUMP=YES
    ;;
    --expandHost)
    EXPAND=YES
    ;;
    --host)
    HOST="$2"
    shift
    ;;
    --port)
    PORT="$2"
    shift
    ;;
    --type)
    TYPE="$2"
    shift
    ;;
    --name)
    NAME="$2"
    shift
    ;;
    --bucket)
    BUCKET="$2"
    shift
    ;;
    *)
      # unknown option
    ;;
esac
shift
done


if [[ -z ${HOST} || -z ${PORT} || -z ${TYPE} || -z ${NAME} || -z ${BUCKET} ]]; then
    echo -e "Usage:\n$0 [--resurrect|--save] --bucket BUCKETNAME --host IP --port PORT --type TYPE --name NAME" && exit 1
fi

if [ "$TYPE" != "mongo" ]; then echo "Unknown type" && exit 2; fi

if [ "$DUMP" = "YES" ]; then
  echo "Starting savior save of ${NAME}"
  ${SAVIORS_DIR}/${TYPE}.sh --save ${EXPAND:+--expandHost }--host "${HOST}" --port "${PORT}" --toFile "${DATA_DIR}/${NAME}.gz"
  if [ "$?" != "0" ]; then
    ${SAVIORS_DIR}/sendmail.sh "The savior for ${NAME} could not save data" "The command :
    ${TYPE}.sh --save ${EXPAND:+--expandHost }--host ${HOST} --port ${PORT} --toFile ${DATA_DIR}/${NAME}.gz
    returned an error code. Check logs for more informations"
    exit 1
  fi
  echo "Sending to ${NAME} s3://${BUCKET}/${NAME}.gz"
  ${SAVIORS_DIR}/s3.sh cp "${DATA_DIR}/${NAME}.gz" "s3://${BUCKET}/${NAME}.gz"
  if [ "$?" != "0" ]; then
    ${SAVIORS_DIR}/sendmail.sh "The savior for ${NAME} could not upload data to S3" "The command :
    s3.sh cp "${DATA_DIR}/${NAME}.gz" s3://${BUCKET}/${NAME}.gz
    returned an error code. Check logs for more informations"
    exit 1
  else
    ${SAVIORS_DIR}/sendmail.sh "The savior for ${NAME} saved the data." "The size of saved data archive is
    $(ls -la ${DATA_DIR}/${NAME}.gz)
    and it has been uploaded to s3://${BUCKET}/${NAME}.gz"
  fi


elif [ "$RESTORE" = "YES" ]; then
  echo "Downloading s3://${BUCKET}/${NAME}.gz to ${NAME}"
  ${SAVIORS_DIR}/s3.sh cp "s3://${BUCKET}/${NAME}.gz" "${DATA_DIR}/${NAME}.gz"
  if [ "$?" != "0" ]; then
    ${SAVIORS_DIR}/sendmail.sh "The savior for ${NAME} could not download data from S3" "The command :
    s3.sh cp s3://${BUCKET}/${NAME}.gz "${DATA_DIR}/${NAME}.gz"
    returned an error code. Check logs for more informations"
    exit 1
  fi
  echo "Starting resurrect of ${NAME}"
  ${SAVIORS_DIR}/${TYPE}.sh --resurrect ${EXPAND:+--expandHost }--host "${HOST}" --port "${PORT}" --fromFile "${DATA_DIR}/${NAME}.gz"
  if [ "$?" != "0" ]; then
    ${SAVIORS_DIR}/sendmail.sh "The savior for ${NAME} could not resurrected data" "The command :
    ${TYPE}.sh --resurrect ${EXPAND:+--expandHost }--host ${HOST} --port ${PORT} --fromFile ${DATA_DIR}/${NAME}.gz
    returned an error code. Check logs for more informations"
    exit 1
  else
    ${SAVIORS_DIR}/sendmail.sh "The savior for ${NAME} resurrected data." "The size of resurrected data archive is
    $(ls -la ${DATA_DIR}/${NAME}.gz)"
  fi

fi
