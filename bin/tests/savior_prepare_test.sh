#!/bin/bash

export TEST_DIR=/tmp/saviortest
export CRONDFILE=${TEST_DIR}/cronfile
basedir=$(dirname $(realpath $0&))

mkdir -p $TEST_DIR

savior_prepare=$basedir/../savior_prepare.sh

function unsetAll {
    unset $(env | grep "SAVIOR.*" | cut -d= -f1)
    rm -f ${CRONDFILE}
}
source "${basedir}/bunit.sh"

echo "Starting tests"

###################################################################################
init whenHostPortAndTypeAreAvailableThenCreateCronEntry
export SAVIOR_MONGO1_HOST="10.10.42.10"
export SAVIOR_MONGO1_PORT="27017"
export SAVIOR_MONGO1_TYPE="mongo"
export SAVIOR_MONGO1_CRON="* * * * * *"
export SAVIOR_MONGO1_BUCKET="saviorsbucket"

eval $savior_prepare

assertThat "${CRONDFILE}" contains "* * * * * * /usr/local/bin/savior.sh --save --type \"mongo\" --bucket \"saviorsbucket\" --host \"10.10.42.10\" --port \"27017\" --name \"MONGO1\""


###################################################################################
init whenHostPortAndTypeAndExpandAreAvailableThenCreateCronEntry
export SAVIOR_MONGO1_HOST="a.fqdn.com"
export SAVIOR_MONGO1_EXPAND="true"
export SAVIOR_MONGO1_PORT="27017"
export SAVIOR_MONGO1_TYPE="mongo"
export SAVIOR_MONGO1_CRON="* * * * * *"
export SAVIOR_MONGO1_BUCKET="saviorsbucket"

eval $savior_prepare

assertThat "${CRONDFILE}" contains "* * * * * * /usr/local/bin/savior.sh --save --expandHost --type \"mongo\" --bucket \"saviorsbucket\" --host \"a.fqdn.com\" --port \"27017\" --name \"MONGO1\""


###################################################################################
init whenMissingCronPropertyReturnError
export SAVIOR_MONGO1_HOST="10.10.42.10"
export SAVIOR_MONGO1_PORT="27017"
export SAVIOR_MONGO1_TYPE="mongo"

assertThat $savior_prepare exitedWith 1


###################################################################################
init whenMissingHostPropertyReturnError
export SAVIOR_MONGO1_CRON="* * * * * *"
export SAVIOR_MONGO1_PORT="27017"
export SAVIOR_MONGO1_TYPE="mongo"

assertThat $savior_prepare exitedWith 1


###################################################################################
init whenMissingPortPropertyReturnError
export SAVIOR_MONGO1_CRON="* * * * * *"
export SAVIOR_MONGO1_HOST="10.10.42.10"
export SAVIOR_MONGO1_TYPE="mongo"

assertThat $savior_prepare exitedWith 1


###################################################################################
init whenMissingTypePropertyReturnError
export SAVIOR_MONGO1_CRON="* * * * * *"
export SAVIOR_MONGO1_HOST="10.10.42.10"
export SAVIOR_MONGO1_PORT="27017"

assertThat $savior_prepare exitedWith 1


###################################################################################
###################################################################################
echo -e "\e[32m✔\e[39m All $totaltest tests passed"
echo -e "Cleaning test files"
rm -rf "$TEST_DIR"
