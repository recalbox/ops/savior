#!/bin/bash

export TEST_DIR=/tmp/saviortest
mkdir -p $TEST_DIR
export MOCK_OUTPUT_FILE=${TEST_DIR}/mockresult
basedir=$(dirname $(realpath $0))
mongosavior="${basedir}/../../saviors/mongo.sh"
export TEST_PATH="$basedir/../mocks"

function unsetAll {
    unset $(env | grep "SAVIOR_.*" | cut -d= -f1)
    rm -f ${CRONDFILE} ${MOCK_OUTPUT_FILE}
}
source "$basedir/../bunit.sh"


echo "Starting mongo savior tests"

###################################################################################
init whenCallingSaveWithHostAndPortThenCallMongoDump

$mongosavior --save --host 10.10.42.10 --port 27017 --toFile FILE.gson.gz

assertThat mongodump hasBeenCalledWith "--host 10.10.42.10 --port 27017 --archive=FILE.gson.gz --gzip"


###################################################################################
init whenCallingSaveWithExpandAndReplicaHostThenCallhost2ipsAndMongodumpWithIps

$mongosavior --save --expandHost --host sr0/a.fqdn.com --port 27017 --toFile FILE.gson.gz

assertThat host2ips.sh hasBeenCalledWith "--separator , --port 27017 a.fqdn.com"
assertThat mongodump hasBeenCalledWith "--host sr0/ip1:port,ip2:port,ip3:port --archive=FILE.gson.gz --gzip"


###################################################################################
init whenCallingSaveWithExpandThenCallhost2ipsAndMongodumpWithIps

$mongosavior --save --expandHost --host a.fqdn.com --port 27017 --toFile FILE.gson.gz

assertThat host2ips.sh hasBeenCalledWith "--separator , --port 27017 a.fqdn.com"
assertThat mongodump hasBeenCalledWith "--host ip1:port,ip2:port,ip3:port --archive=FILE.gson.gz --gzip"


resurectFile="${TEST_DIR}/FILE.gson.gz"
touch "${resurectFile}"
###################################################################################
init whenCallingResurrectWithHostAndPortThenCallMongoRestore

$mongosavior --resurrect --host 10.10.42.10 --port 27017 --fromFile ${resurectFile}

assertThat mongorestore hasBeenCalledWith "--host 10.10.42.10 --port 27017 --archive=${resurectFile} --gzip"


###################################################################################
init whenCallingResurrectWithExpandAndReplicaHostThenCallhost2ipsAndMongoRestoreWithIps

$mongosavior --resurrect --expandHost --host sr0/a.fqdn.com --port 27017 --fromFile ${resurectFile}

assertThat host2ips.sh hasBeenCalledWith "--separator , --port 27017 a.fqdn.com"
assertThat mongorestore hasBeenCalledWith "--host sr0/ip1:port,ip2:port,ip3:port --archive=${resurectFile} --gzip"


###################################################################################
init whenCallingResurrectWithExpandThenCallhost2ipsAndMongoRestoreWithIps

$mongosavior --resurrect --expandHost --host a.fqdn.com --port 27017 --fromFile ${resurectFile}

assertThat host2ips.sh hasBeenCalledWith "--separator , --port 27017 a.fqdn.com"
assertThat mongorestore hasBeenCalledWith "--host ip1:port,ip2:port,ip3:port --archive=${resurectFile} --gzip"



###################################################################################
###################################################################################
echo -e "\e[32m✔\e[39m All $totaltest tests passed"
echo -e "Cleaning test files"
rm -rf "$TEST_DIR"
