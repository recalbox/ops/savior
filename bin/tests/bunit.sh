#!/bin/bash

totaltest=0

function init {
  currenttest=$1
  unsetAll
}
function assertThat {
    totaltest=$((totaltest+1))
    case $2 in
    contains)
      if test "$(echo -e "$3")" != "$(cat $1)"; then
        echo -e "\e[31m❌\e[39m Test failed: $currenttest"
        echo -e "Expected string: \n$3\ndo not equals to $1 content: \n$(cat $1)"
        exit 1
      fi
      ;;
    containsNumberOfLine)
      if test $3 -ne $(cat $1 | wc -l); then
        echo -e "\e[31m❌\e[39m Test failed: $currenttest"
        echo -e "Expected number of lines: \n$3\ndo not equals to $1 number of lines: \n$(cat $1 | wc -l)"
        exit 1
      fi
      ;;
    exitedWith)
      eval "$1"
      returned=$?
      if test $returned -ne $3; then
        echo -e "\e[31m❌\e[39m Test failed: $currenttest"
        echo -e "Expected $1 to return: \n$3\nbut returned:\n$returned"
        exit 1
      fi
      ;;
    hasBeenCalledWith)
      if ! cat $MOCK_OUTPUT_FILE | grep -q "$1 $3"; then
        echo -e "\e[31m❌\e[39m Test failed: $currenttest"
        echo -e "Expected $1 to have been called with: \n$1 $3\nBut has been called:\n$(cat $MOCK_OUTPUT_FILE | grep $1)"
        exit 1
      fi
      ;;
    hasBeenCalled)
      if ! cat $MOCK_OUTPUT_FILE | grep -q "$1"; then
        echo -e "\e[31m❌\e[39m Test failed: $currenttest"
        echo -e "Expected $1 to have been called"
        exit 1
      fi
      ;;
    *)
      echo "Assertion unknown" && echo -e "\n\n\e[31m❌\e[39m Tests failed"; exit 1
      ;;
    esac
}
