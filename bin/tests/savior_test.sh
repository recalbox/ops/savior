#!/bin/bash

export TEST_DIR=/tmp/saviortest
mkdir -p $TEST_DIR
export MOCK_OUTPUT_FILE=${TEST_DIR}/mockresult
basedir=$(dirname $(realpath $0))
savior="${basedir}/../savior.sh"
export SAVIORS_DIR="$basedir/mocks"
export DATA_DIR=${TEST_DIR}

function unsetAll {
    unset $(env | grep "SAVIOR_.*" | cut -d= -f1)
    rm -f ${CRONDFILE} ${MOCK_OUTPUT_FILE}
}
source "$basedir/bunit.sh"


echo "Starting SAVE tests"

###################################################################################
init whenCallingSaveWithMongoAndHostThenCallMongoSavior

$savior --save --bucket saviors --type mongo --host 10.10.42.10 --port 27017 --name MONGO1

assertThat mongo.sh hasBeenCalledWith "--save --host 10.10.42.10 --port 27017 --toFile ${DATA_DIR}/MONGO1.gz"
assertThat s3.sh hasBeenCalledWith "cp ${DATA_DIR}/MONGO1.gz s3://saviors/MONGO1.gz"

###################################################################################
init whenCallingSaveWithMongoAndHostThenCallMongoSaviorAndSendMail

$savior --save --bucket saviors --type mongo --host 10.10.42.10 --port 27017 --name MONGO1

assertThat sendmail.sh hasBeenCalled


###################################################################################
init whenCallingSaveWithUnexistingTypeThenReturnsError

assertThat "$savior --save --bucket saviors --type unexisting --host 10.10.42.10 --port 27017 --name MONGO1" exitedWith 2


###################################################################################
init whenCallingSaveWithoutBucketThenReturnsError

assertThat "$savior --save --type unexisting --host 10.10.42.10 --port 27017 --name MONGO1" exitedWith 1


###################################################################################
init whenCallingSaveWithUnusableVariablesThenSendAMail

$savior --save --bucket saviors --type mongo --host "10.10.42.10 lol" --port 27017 --name MONGO1

assertThat mongo.sh hasBeenCalledWith "--save --host 10.10.42.10 lol --port 27017 --toFile ${DATA_DIR}/MONGO1.gz"
assertThat sendmail.sh hasBeenCalled


###################################################################################
init whenCallingSaveWithMongoAndHostThenCallMongoSavior

$savior --save --bucket saviors --type mongo --host 10.10.42.10 --port 27017 --name MONGO1

assertThat mongo.sh hasBeenCalledWith "--save --host 10.10.42.10 --port 27017 --toFile ${DATA_DIR}/MONGO1.gz"
assertThat s3.sh hasBeenCalledWith "cp ${DATA_DIR}/MONGO1.gz s3://saviors/MONGO1.gz"


###################################################################################
init whenCallingSaveWithMongoAndHostThenCallMongoSaviorAndSendMail

$savior --save --bucket saviors --type mongo --host 10.10.42.10 --port 27017 --name MONGO1

assertThat sendmail.sh hasBeenCalled


###################################################################################
init whenCallingSaveWithUnexistingTypeThenReturnsError

assertThat "$savior --save --bucket saviors --type unexisting --host 10.10.42.10 --port 27017 --name MONGO1" exitedWith 2


###################################################################################
init whenCallingSaveWithoutBucketThenReturnsError

assertThat "$savior --save --type mongo --host 10.10.42.10 --port 27017 --name MONGO1" exitedWith 1


###################################################################################
init whenCallingSaveWithUnusableVariablesThenSendAMail

$savior --save --bucket saviors --type mongo --host "10.10.42.10 lol" --port 27017 --name MONGO1

assertThat mongo.sh hasBeenCalledWith "--save --host 10.10.42.10 lol --port 27017 --toFile ${DATA_DIR}/MONGO1.gz"
assertThat sendmail.sh hasBeenCalled


###################################################################################
init whenCallingSaveWithExpandHostThenCallMongoSaviorWithExpand

$savior --save --expandHost --bucket saviors --type mongo --host "a.fqdn.com" --port 27017 --name MONGO1

assertThat mongo.sh hasBeenCalledWith "--save --expandHost --host a.fqdn.com --port 27017 --toFile ${DATA_DIR}/MONGO1.gz"
assertThat sendmail.sh hasBeenCalled


echo "Starting RESURRECT tests"

###################################################################################
init whenCallingResurrectWithMongoAndHostThenCallMongoSavior

$savior --resurrect --bucket saviors --type mongo --host 10.10.42.10 --port 27017 --name MONGO1

assertThat s3.sh hasBeenCalledWith "cp s3://saviors/MONGO1.gz ${DATA_DIR}/MONGO1.gz"
assertThat mongo.sh hasBeenCalledWith "--resurrect --host 10.10.42.10 --port 27017 --fromFile ${DATA_DIR}/MONGO1.gz"

###################################################################################
init whenCallingResurrectWithMongoAndHostThenCallMongoSaviorAndSendMail

$savior --resurrect --bucket saviors --type mongo --host 10.10.42.10 --port 27017 --name MONGO1

assertThat sendmail.sh hasBeenCalled


###################################################################################
init whenCallingResurrectWithUnexistingTypeThenReturnsError

assertThat "$savior --resurrect --bucket saviors --type unexisting --host 10.10.42.10 --port 27017 --name MONGO1" exitedWith 2


###################################################################################
init whenCallingResurrectWithoutBucketThenReturnsError

assertThat "$savior --resurrect --type mongo --host 10.10.42.10 --port 27017 --name MONGO1" exitedWith 1


###################################################################################
init whenCallingResurrectWithUnusableVariablesThenSendAMail

$savior --resurrect --bucket saviors --type mongo --host "10.10.42.10 lol" --port 27017 --name MONGO1

assertThat mongo.sh hasBeenCalledWith "--resurrect --host 10.10.42.10 lol --port 27017 --fromFile ${DATA_DIR}/MONGO1.gz"
assertThat sendmail.sh hasBeenCalled


###################################################################################
init whenCallingResurrectWithExpandHostThenCallMongoSaviorWithExpand

$savior --resurrect --expandHost --bucket saviors --type mongo --host "a.fqdn.com" --port 27017 --name MONGO1

assertThat mongo.sh hasBeenCalledWith "--resurrect --expandHost --host a.fqdn.com --port 27017 --fromFile ${DATA_DIR}/MONGO1.gz"
assertThat sendmail.sh hasBeenCalled



###################################################################################
###################################################################################
echo -e "\e[32m✔\e[39m All $totaltest tests passed"
echo -e "Cleaning test files"
rm -rf "$TEST_DIR"
