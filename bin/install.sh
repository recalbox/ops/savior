#!/bin/bash -e

if [ ! -f /.installed ];then
  envsubst < /usr/local/conf/ssmtp.conf.template > /etc/ssmtp/ssmtp.conf
  echo "root:$SSMTP_EMAIL" > /etc/ssmtp/revaliases
  savior_prepare.sh
  touch /.installed
  echo "Savior installation success"
fi

crond -f