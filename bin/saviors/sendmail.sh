#!/bin/bash
if [[ -n "$EMAIL_DEST" ]];then
sendmail -F "${SSMTP_FULLNAME:-$SSMTP_EMAIL}" "$EMAIL_DEST" <<EOF
subject:$1

$2
EOF
fi
