#!/bin/bash -e

export PATH=${TEST_PATH:-$PATH}

while [[ $# -gt 1 ]]
do
key="$1"
case $key in
    --resurrect)
    RESTORE=YES
    ;;
    --save)
    SAVE=YES
    ;;
    --expandHost)
    EXPAND=YES
    ;;
    --host)
    HOST="$2"
    shift
    ;;
    --port)
    PORT="$2"
    shift
    ;;
    --toFile)
    OUTPUT="$2"
    shift
    ;;
    --fromFile)
    INPUT="$2"
    shift
    ;;
    *)
      # unknown option
    ;;
esac
shift
done

if [[ -z ${HOST} || -z ${PORT} ]]; then
    echo -e "Usage:\n$0 [--resurrect|--save] --expandHost --host HOST [--port PORT] [--toFile|--fromFile] FILE.gson.gz" && exit 1
fi

if [ "${EXPAND}" == "YES" ];then
  [ $DEBUG ] && echo "Expanding host $HOST and removing --port from command"
  if [[ "${HOST}" == *"/"* ]]; then
    ARGS=(${HOST//// })
    EXPANDED_HOSTS="${ARGS[0]}/"
    HOST="${ARGS[1]}"
    [ $DEBUG ] && echo "Detected replica name $EXPANDED_HOSTS"

  fi
  [ $DEBUG ] && echo "Using host2ips.sh --separator "," --port ${PORT} ${HOST}"
  HOST="${EXPANDED_HOSTS}$(host2ips.sh --separator "," --port "${PORT}" "${HOST}")"
  PORT=""
fi

if [ "${RESTORE}" == "YES" ];then
  if [[ -z ${INPUT} || ! -f ${INPUT} ]]; then
    echo -e "Usage:\n$0 --resurrect --expandHost --host HOST [--port PORT] --fromFile FILE.gson.gz" && exit 1
  fi
  echo "Restoring database to ${HOST}"
  mongorestore --host ${HOST} ${PORT:+--port $PORT }--archive="${INPUT}" --gzip
elif [ "${SAVE}" == "YES" ];then
  if [[ -z ${OUTPUT} ]]; then
    echo -e "Usage:\n$0 --save --expandHost --host HOST [--port PORT] --toFile FILE.gson.gz" && exit 1
  fi
  echo "Saving database from $HOST"
  mongodump --host "${HOST}" ${PORT:+--port $PORT }--archive="${OUTPUT}" --gzip
fi

