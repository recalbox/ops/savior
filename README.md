Savior
======

Your container data savior.

Dump databases and upload archives to a S3 compatible service bucket.

Send mails on events.

Standard configuration:
* `DEBUG`: set to 'true' if you want debug log
* `AWS_SECRET_ACCESS_KEY`: your amazon ACCESS
* `ENV AWS_ACCESS_KEY_ID`: your amazon SECRET
* `EMAIL_DEST`: email to send events
* `SSMTP_DOMAIN`: sender email domain 
* `SSMTP_EMAIL`: sender email
* `SSMTP_FULLNAME`: sender FROM (use SSMTP_EMAIL if blank)
* `SSMTP_HOST`: smtp relay host
* `SSMTP_PORT`: smtp relay port
* `SSMTP_PASSWORD`: smtp password

To add a Savior, use savior environment variables.
The savior env var are composed by `SAVIOR_[SAVIORNAME]_[OPTION]`.


* `SAVIOR_XXXX_HOST`: the host to save. Can be multiple hosts for mongo (e.g.: `rs0/host1,host2`)
* `SAVIOR_XXXX_EXPAND`: use nslookup to get DNS A entries from `SAVIOR_XXXX_HOST`
* `SAVIOR_XXXX_PORT`: the port to contact
* `SAVIOR_XXXX_TYPE`: the type of savior (mongo)
* `SAVIOR_XXXX_CRON`: the cron style scheduling 
* `SAVIOR_XXXX_BUCKET`: the name of the bucket to use when uploading to S3 compatible service
 

## Save
For example to create a savior for a mongo database, that will dump the base at midnight:
```bash
docker run -ti --rm \
-e AWS_SECRET_ACCESS_KEY="XXXXXXXX" \
-e AWS_ACCESS_KEY_ID="YYYYYYYYYY" \
-e SAVIOR_MONGO_RECALBOX_TYPE="mongo" \
-e SAVIOR_MONGO_RECALBOX_HOST=192.168.1.39 \
-e SAVIOR_MONGO_RECALBOX_PORT=27017 \
-e SAVIOR_MONGO_RECALBOX_CRON="0 0 * * *" \
-e SAVIOR_MONGO_RECALBOX_BUCKET="recalbox-test" \
registry.gitlab.com/recalbox/ops/savior:latest

```

Full example with email support and a mongo savior dumping the base at 01:00 and 13:00: 
```bash
docker run -ti --rm \
-e AWS_SECRET_ACCESS_KEY="XXXXXXXX" \
-e AWS_ACCESS_KEY_ID="YYYYYYYYYY" \
-e EMAIL_DEST="admin@yourdomain.com" \
-e SSMTP_DOMAIN="yourdomain.com" \
-e SSMTP_EMAIL="rancher@yourdomain.com" \
-e SSMTP_HOST="mail.whatever.net" \
-e SSMTP_PORT=587 \
-e SSMTP_PASSWORD="mysmtppassword" \
-e SAVIOR_MONGO_RECALBOX_TYPE="mongo" \
-e SAVIOR_MONGO_RECALBOX_HOST="192.168.1.39" \
-e SAVIOR_MONGO_RECALBOX_PORT=27017 \
-e SAVIOR_MONGO_RECALBOX_CRON="0 01,13 * * *" \
-e SAVIOR_MONGO_RECALBOX_BUCKET="saviors" \
registry.gitlab.com/recalbox/ops/savior:latest
```


## Resurrect
To restore a database from a previous save, you have to use the savior in command line.

This will download and restore the MONGO1 save:
```bash
savior.sh --resurrect --bucket saviors --type mongo --host 10.10.42.10 --port 27017 --name MONGO1
```

