FROM alpine:3.6

ARG DOCKER_VERSION=17.05.0-ce
ENV AWS_SECRET_ACCESS_KEY=""
ENV AWS_ACCESS_KEY_ID=""

ENV EMAIL_DEST ''
ENV SSMTP_DOMAIN ''
ENV SSMTP_EMAIL ''
ENV SSMTP_HOST ''
ENV SSMTP_PORT ''
ENV SSMTP_PASSWORD ''
ENV SSMTP_FULLNAME ''

RUN apk -v --update add \
        mongodb-tools \
        bash \
        python \
        py-pip \
        groff \
        less \
        mailcap \
        curl \
        ssmtp \
        gettext \
        && \
    pip install --upgrade awscli s3cmd python-magic && \
    apk -v --purge del py-pip && \
    rm /var/cache/apk/*

RUN curl https://get.docker.com/builds/Linux/x86_64/docker-${DOCKER_VERSION}.tgz | tar -xz --strip 1 -C /usr/local/bin/ && chmod +x /usr/local/bin/docker

ADD bin /usr/local/bin

ADD conf/* /usr/local/conf/

ADD bin/tests/ /usr/local/bin/tests

RUN /usr/local/bin/tests/testsuite.sh

RUN mkdir /saviors/

CMD install.sh