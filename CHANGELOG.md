# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.2] - 2017-09-17
### Fixed
- restart container do not add a line in crontab

## [1.0.1] - 2017-07-22
### Fixed
- from field for email

## [1.0.0] - 2017-07-01
### Added
- crond scheduling for saviors
- savior for mongodb
- email alerts
- gitlab ci
- added resurrect command for savior
- added EXPAND variable for dns resolve at runtime
- added DEBUG variable
